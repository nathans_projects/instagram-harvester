import json

from Chrome import Chrome

with open('./crawler/instagram_objects.json') as f:
    instagram_objects = json.load(f)

chrome = Chrome()
chrome.set_options(False).get_driver(full_screen=False)

chrome.navigate('https://www.instagram.com')

login_objects = instagram_objects['login_page']

login_link = login_objects['login_link']
chrome.click(link_text=login_link['text'])


mail_field = login_objects['mail_field']
chrome.wait_for(obj=mail_field['text'])
chrome.send_keys('nathangavenski@gmail.com', name=mail_field['name'])

passward_field = login_objects['password_field']
chrome.send_keys('', name=passward_field['name'])

# home_objects = instagram_objects['home_page']
# search_field = home_objects['search_field']
# chrome.send_keys('#hotdog', obj_class=search_field['class'])